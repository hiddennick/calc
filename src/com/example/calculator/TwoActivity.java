package com.example.calculator;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by лгпен on 05.02.2015.
 */
public class TwoActivity extends Activity {
    public TextView outPutInfo;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.two);
        outPutInfo=(TextView)findViewById(R.id.tvView);
        int outVariable=addition();
        outPutInfo.setText( getIntent().getStringExtra("text_1") + "+" + getIntent().getStringExtra("text_2") + "=" + outVariable);
    }
    public int addition(){
        int first=Integer.valueOf( getIntent().getStringExtra("text_1"));
        int second=Integer.valueOf(getIntent().getStringExtra("text_2"));
        int result= first+second;
        return result;
    }
}