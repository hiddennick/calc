package com.example.calculator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MyActivity extends Activity {
    public EditText text_1;
    public EditText text_2;
    public Button click;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        text_1=(EditText)findViewById(R.id.editText);
        text_2=(EditText)findViewById(R.id.editText2);
        click = (Button) findViewById(R.id.button);
        data_transmission();
    }
    private boolean isValidate(){
        boolean flag=true;
        if(text_1.getText().length()==0){
            text_1.setError(getString(R.string.error1));
            flag=false;
        }
        if(text_2.getText().length()==0){
            text_2.setError(getString(R.string.error2));
            flag=false;
        }
        return flag;

    }
    public void data_transmission(){
        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isValidate()){
                    Intent intent = new Intent(MyActivity.this, TwoActivity.class);
                    intent.putExtra("text_1", text_1.getText().toString());
                    intent.putExtra("text_2", text_2.getText().toString());
                    startActivity(intent);}
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}

